# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ config, pkgs, ... }:

{

  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  boot = {
    loader = {
      efi = {
	canTouchEfiVariables = true ;
        efiSysMountPoint = "/boot/efi" ;
      } ;
      grub = {
        enable = true;
        device = "nodev" ;  #  "/dev/device" for BIOS or "nodev" for efi
        efiSupport = true ;
        #efiInstallAsRemovable = true ;
      } ;
    } ;
  } ;

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    # keyMap = "us";
    useXkbConfig = true; # use xkbOptions in tty.
  };

  containers = let
    containerConfig = {
      privateNetwork = true ;
      config = { config, pkgs, ... }: {
	environment.etc."resolv.conf".text = "nameserver 1.1.1.1" ;
	users.users.mdp = {
	  isNormalUser = true ;
	  home = "/home/mdp" ;
	  extraGroups = [ "wheel" ] ;
	  packages = with pkgs ; [
	    neofetch
	  ] ;
        } ;

        networking.firewall = {
	  enable = true ;
	  allowedTCPPorts = [ 80 ] ;
        } ;

        system.stateVersion = "22.05" ;
      } ;
    } ;
  in {
    dev0 = ( containerConfig // {
      autoStart = true ;
      hostAddress = "192.168.122.10" ;
      localAddress = "192.168.122.11" ;
    } ) ;

    dev1 = ( containerConfig // {
      hostAddress = "192.168.122.12" ;
      localAddress = "192.168.122.13" ;
    } ) ;

    dev2 = ( containerConfig // {
      hostAddress = "192.168.122.14" ;
      localAddress = "192.168.122.15" ;
    } ) ;
  } ;

  networking = {
    hostName = "nixos" ;
    wireless = {
      enable = true ;  #  wpa_supplicant
      networks = {
        vodafoneABB902 = {
	  psk = "wH7RN7hbUqHuLtRn" ;
	} ;
      } ;
    } ;
    nat = {
      enable = true ;
      internalInterfaces = [ "vn1" ] ;
      externalInterface = "enp1s0" ;
      enableIPv6 = true ;
    } ;
    networkmanager = {
      enable = false ;
    } ;
    proxy = {
      # default = "http://user:password@proxy:port/" ;
      # noProxy = "127.0.0.1,localhost,internal.domain" ;
    } ;
    firewall = {
      allowedTCPPorts = [ 6443 ];
      # allowedUDPPorts = [ ... ];
      # Or disable the firewall altogether.
      # enable = false;
    } ;
  } ;

  nixpkgs.config.allowUnfree = true ;

  time = {
    timeZone = "Pacific/Auckland" ;
  } ;

  services = {
  	# Enable the OpenSSH daemon.
    openssh = {
      enable = true;
    } ;
    k3s = {
      enable = true ;
      role = "server" ;
    } ;
    printing = {
      enable = false ;
    } ;
    xserver = {
      enable = true ;
      layout = "us" ;
      xkbOptions = "ctrl:caps" ;
      libinput = {
        enable = true ; 
      } ;
      displayManager = {
        sddm.enable = true ;
      } ;
      windowManager = {
        qtile.enable = true ;
      } ;
      resolutions = [
        {
          x = 1366 ;
          y = 768 ;
        }
      ] ;
      virtualScreen = {
        x = 1366 ;
        y = 768 ;
      } ;
    } ;
  } ;

  sound = {
    enable = true ;
  } ;

  specialisation = {
    tor.configuration = {
    } ;
  } ;

  hardware = {
    pulseaudio = {
      enable = true ;
      support32Bit = true ;
      configFile = pkgs.runCommand "default.pa" {} ''
        sed 's/module-udev-detect$/module-udev-detect tsched=0/' \
	  ${pkgs.pulseaudio}/etc/pulse/default.pa > $out
      '' ;
    } ;
  } ;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.mdp = {
    isNormalUser = true;
    home = "/home/mdp" ;
    extraGroups = [
        "audio"
	"libvirtd"
	"video"
        "wheel"  # Enable ‘sudo’ for the user.
    ];
    packages = with pkgs; [
	alacritty
	discord
	dunst
	feh
	gimp
	git
	libnotify
	neofetch
	picom
	polybar
	pulsemixer
	qutebrowser
	rofi
	spotify
    ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
     k3s
     killall
     neovim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
     ( python38.withPackages( ps: with ps; [
       pandas
     ] ) )
     virt-manager
  #   wget
  ];

  # Virtualization
  virtualisation.libvirtd.enable = true ;
  programs.dconf.enable = true ;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  system = {
    copySystemConfiguration = true;
    stateVersion = "23.05"; # Don't change
  } ;

}
